<?php

namespace App\Controller;

use App\Service\DataService;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;



class DataController extends AbstractController
{
    private $dataService;
    public function __construct(DataService $dataService)
    {
        $this->dataService = $dataService;
    }


    /**
     * @Route ("/",name="homepage")
     * @Route("/data", name="data")
     */
    public function index()
    {
        $client = HttpClient::create();
        //$client = new CurlHttpClient();
        $uri = "https://j8a44btxhb.execute-api.eu-west-1.amazonaws.com/dev/results";
        $response = $client->request('GET', $uri);
        $msg = json_decode($response->getContent(),true);


        return new Response($this->dataService->fetchResults($msg));
    }
}
