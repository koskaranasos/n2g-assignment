<?php


namespace App\Consumer;


use App\Entity\Dataset;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class DataConsumer implements ConsumerInterface
{
    private $em;
    private $logger;
    private $validator;
    public function __construct(EntityManagerInterface $em, LoggerInterface $logger, ValidatorInterface $validator)
    {
        $this->em=$em;
        $this->logger=$logger;
        $this->validator=$validator;
    }

    /**
     * The core function of the consumer. Gets triggered whenever a consumer is initiated.
     * Gets a AMPQMessage type of message from the queue.
     * @param AMQPMessage $msg
     */
    public function execute(AMQPMessage $msg)
    {
        $body = json_decode($msg->body,true); //decoding to array
        $this->insertToDB($body);  //calls the function of the consumed message in order to be inserted in the DB.

    }

    /**
     * Initiates a new dataset object and inserts into the DB the elements of the consumed message.
     * @param $message
     */
    private function insertToDB($message)
    {
        $dataset = new Dataset(); //initiating new Dataset object
        $dataset->setValue($message['value']); //setting the value
        $dataset->setTimestamp($message['timestamp']); //setting the timestamp

        $error = $this->validator->validate($dataset); //validating the changes
        if (count($error) > 0) {
            $this->logError($message, $error);
        }
        else {
            $this->em->persist($dataset);
            $this->em->flush();
            echo('Data with value ' . $dataset->getValue() . ' and timestamp ' . $dataset->getTimestamp() . ' have been added to the table.'."\n");
        }
    }
    /**
     * Logs error message in case of a validation error before inserting to DB.
     * Takes the original message as a parameter and the validation error message
     * @param $message
     * @param $error
     */
    private function logError($message, $error)
    {
        $data = [
            'error' => $error,
            'message' => $message
        ];

        $this->logger->error(json_encode($data)); //the error response is being encoded to json format
    }
}