<?php

namespace App\Service;



use App\Consumer\DataConsumer;
use App\Consumer\NewDataConsumer;
use App\Producer\DataProducer;
use App\Producer\NewDataProducer;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;

class DataService
{

    private $dataProducer;
    private $newDataProducer;
    private $newDataConsumer;

    public function __construct(DataProducer $dataProducer, NewDataProducer $newDataProducer, NewDataConsumer $newDataConsumer)
    {
        $this->dataProducer = $dataProducer;
        $this->newDataProducer = $newDataProducer;
        $this->newDataConsumer = $newDataConsumer;
    }

    /**
     * Splits received dataset and calls the producer to insert to the queue.
     * @param $msg
     * @return string
     */
    public function fetchResults($msg)
    {
        $gatewayId = $this->BigNumberHexToDecimal($msg['gatewayEui']);
        $profileId = $this->BigNumberHexToDecimal($msg['profileId']);
        $endpointId = $this->BigNumberHexToDecimal($msg['endpointId']);
        $clusterId = $this->BigNumberHexToDecimal($msg['clusterId']);
        $attributeId = $this->BigNumberHexToDecimal($msg['attributeId']);
        $routing_key = $gatewayId . '.' . $profileId . '.' . $endpointId . '.' . $clusterId . '.' . $attributeId;

        $this->dataProducer->add($msg,$routing_key); //my queue
        //$this->newDataProducer->add($msg,$routing_key); //net2grid queue
        //$this->newDataConsumer->getQueue($routing_key);
        return 'A new message has been added to the queue with value: '. $msg['value']. "\n" .' timestamp: '.$msg['timestamp'] .' and routing key: '.$routing_key. "\n";

        }

    /**
     * Receives a big hexadecimal number and converts it to decimal.
     * @param $hex
     * @return string
     */
    private function BigNumberHexToDecimal($hex)
    {
        $dec = array(0);
        $hexLen = strlen($hex);
        for ($h = 0; $h < $hexLen; ++$h) {
            $carry = hexdec($hex[$h]);
            for ($i = 0; $i < count($dec); ++$i) {
                $val = $dec[$i] * 16 + $carry;
                $dec[$i] = $val % 10;
                $carry = (int)($val / 10);
            }
            while ($carry > 0) {
                $dec[] = $carry % 10;
                $carry = (int)($carry / 10);
            }
        }

        return join("", array_reverse($dec));
    }
}