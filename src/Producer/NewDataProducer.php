<?php


namespace App\Producer;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class NewDataProducer
{
    public function add($msg, $routing_key)
    {
        $connection = new AMQPStreamConnection('candidatemq.n2g-dev.net', 5672, 'cand_739x', 'UOizKA2McvczP0LW');
        $channel = $connection->channel();
        if (!empty($msg)) {

            $message = [
                'value' => $msg['value'],
                'timestamp' => $msg['timestamp']
            ];
        }
        else{
            $message=[
                'value'=>null,
                'timestamp'=>null
            ];
        }
        $newMsg = new AMQPMessage($message);
        $channel->basic_publish($newMsg, 'cand_739x', $routing_key);
        $channel->close();
        $connection->close();
    }

}