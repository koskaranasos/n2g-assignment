<?php


namespace App\Producer;


use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;


class DataProducer
{
    private $producer;
    public function __construct(ProducerInterface $producer){
        $this->producer = $producer;
    }

    /**
     * The core function of the producer.
     * Publishes a new message in the corresponding queue along with the routing key.
     * In case the message is empty from the external API a custom message with null values is being built.
     * The latter case is being handled by the consumer.
     * @param $msg
     * @param $routing_key
     */
    public function add($msg, $routing_key)
    {
        if (!empty($msg)) {

            $message = [
                'value' => $msg['value'],
                'timestamp' => $msg['timestamp']
            ];
        }
        else{
         $message=[
             'value'=>null,
             'timestamp'=>null
         ];
        }
        $this->producer->publish(json_encode($message),$routing_key);

    }

}